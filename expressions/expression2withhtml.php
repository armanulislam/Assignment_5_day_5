
<!doctype html>
<html>
<head>
    <title>
        this is a magic constant
    </title>
</head>
<body>
<?php
function double($i)
{
    return $i*2;
}
$b = $a = 5;
echo "values of b and a is: ".$b.",".$a."(/* assign the value five into the variable a and b */)";
$c = $a++;        echo "values of b and a is: ".$c.",".$a."(/* post-increment, assign original value of a 
                       (5) to c */)";
echo "<hr>";

$e = $d = ++$b;     echo "values of e and d is: ".$e.",".$d."(/* pre-increment, assign the incremented value of 
                       b (6) to d and e */)";

echo"/* at this point, both d and e are equal to 6 */";
echo "<hr>";
$f = double($d++);  echo "values of f and d is: ".$f.",".$d."(/* assign twice the value of d before
                       the increment, 2*6 = 12 to f */)";
echo "<hr>";
$g = double(++$e);  echo "values of g and e is: ".$g.",".$e."(/* assign twice the value of e after
                       the increment, 2*7 = 14 to g */)";
echo "<hr>";
$h = $g += 10;      echo "values of b and a is: ".$h.",".$g."(/* first, g is incremented by 10 and ends with the 
                       value of 24. the value of the assignment (24) is 
                       then assigned into h, and h ends with the value 
                       of 24 as well. */)";
?>
</body>


</html>